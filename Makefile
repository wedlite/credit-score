.PHONY: clean data lint export_env
#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROFILE = default
PROJECT_NAME = credit_score_model
PYTHON_INTERPRETER = python3

#################################################################################
# COMMANDS                                                                      #
#################################################################################


## export intalled package to environtment.yml
export_env:
	conda env export > environment.yml

## Make Training Dataset
data: 
	$(PYTHON_INTERPRETER) src/data/get_dataset.py

## Train the model
train_dr:
	$(PYTHON_INTERPRETER) src/models/train_model.py --model="datarobot"

## Export TokopediaLoanProposals from datastore to google storage
move_loans:
	gcloud config set project taralite-apply
	gcloud auth login
	gcloud datastore export --kinds="tokopediaLoanProposals" --namespaces="tokopedia" 

## Export npwp from datastore to google storage
move_npwp:
	gcloud config set project taralite-ekyc
	gcloud auth login
	gcloud datastore export --kinds="npwp" --namespaces="taralite" gs://ekyc_npwp
	gsutil cp -r gs://ekyc_npwp/* gs://credit_score/npwp

## Export pefindo from datastore to google storage
move_pefindo:
	gcloud config set project taralite-ds
	gcloud auth login
	gcloud datastore export --kinds="Single,Multiple,Report" gs://pefindo
	gsutil cp -r gs://pefindo/* gs://credit_score/pefindo

## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

## Delete all downloaded raw data
clean_raw_data:	
	rm -i -r data/raw/*

## Lint using flake8
lint:
	flake8 src

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
