from sklearn.externals.six import StringIO
from IPython.display import Image
from sklearn.tree import export_graphviz
import pydotplus
from IPython.display import display
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import tree
from sklearn import metrics
import seaborn as sns
plt.style.use('seaborn-notebook')


def show_tree(features, target, max_depth=5, min_samples_split=50, min_impurity_decrease=0.001):
    """ Train a decision tree based on the feature and show the tree """
    clf = tree.DecisionTreeClassifier(criterion='entropy',
                                      class_weight='balanced',
                                      max_depth=max_depth,
                                      min_samples_split=min_samples_split,
                                      min_impurity_decrease=min_impurity_decrease)
    clf = clf.fit(features, target)
    dot_data = StringIO()
    export_graphviz(clf,
                    out_file=dot_data,
                    filled=True,
                    rounded=True,
                    special_characters=True,
                    feature_names=features.columns.values,
                    class_names=["non default", "default"])
    graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
    display(Image(graph.create_png()))
    pred = clf.predict_proba(features)[:, 1]
    auc = metrics.roc_auc_score(target, pred)
    print("auc score : {}".format(auc))


def plot_bar(data, col_name, xlabel, norm='columns', barWidth=0.25, size=(14, 6)):
    """ show plot bar based on the category proportion with the default status """
    tabNormed = pd.crosstab(data[col_name], data['default'],
                            normalize=norm).sort_values(col_name,
                                                        ascending=True).reset_index()
    plt.figure(figsize=size)
    fig, ax = plt.subplots(figsize=size)
    r1 = np.arange(len(tabNormed[1.0]))
    r2 = [x + barWidth for x in r1]
    p1 = ax.bar(r1, tabNormed[1.0], barWidth, label='Default')
    p2 = ax.bar(r2, tabNormed[0.0], barWidth, label='Not Default')
    plt.xlabel('{}'.format(xlabel), fontweight='bold')
    plt.ylabel('Proportion of default', fontweight='bold')
    plt.xticks([r + barWidth for r in range(len(tabNormed[1.0]))],
               list(tabNormed[col_name]))
    plt.legend()
    plt.show()


def plot_hist_bar(data, col_name, xlabel):
    """ plot data distribution according to the default """
    bar_data = data.groupby([col_name]).size().reset_index()
    bar_data.iloc[:, 1] = bar_data.iloc[:, 1] / sum(bar_data.iloc[:, 1])
    fig = sns.barplot(x=col_name, y=0, data=bar_data)
    plt.xlabel(xlabel)
    plt.ylabel('Percentage of loans', fontweight='bold')
    plt.show(fig)

def plot_linreg(x,y,intercept,slope):
    """ plot linear regression """
    plt.plot(x, y, 'o', label='original data')
    plt.plot(x, intercept + slope*x, 'r', label='fitted line')
    plt.legend()
    plt.show()