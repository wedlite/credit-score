import datarobot as dr
import pickle
import click
import pandas as pd
import logging
from src.data.get_predict_data import PredictDataset

# datarobot token
dr.Client(config_path='/mnt/drconfig.yaml')

def predict_dr(id):
    """ call datarobot prediction based on selected model """
    logger = logging.getLogger(__name__)
    predict_data = PredictDataset(id)
    predict_data = predict_data.get_clean_data()
    result = 'no data based on {}'.format(id)
    if(len(predict_data) > 0):
        with open('/mnt/results/models/selected_model.pkl', 'rb') as sel_model:
            selected_model = pickle.load(sel_model)
        logger.info("getting predictions")
        project = dr.Project.get(selected_model.project_id)
        prediction_dataset = project.upload_dataset(predict_data)
        predict_job = selected_model.request_predictions(prediction_dataset.id)
        predictions = predict_job.get_result_when_complete()
        label_predictions = pd.cut(predictions['positive_probability'], [0,0.025,0.05,0.075,0.1,0.125,0.15,1], 
        labels=["A1","A2","B1", "B2", "C1", "C2", "default"], 
        include_lowest=True)
        result = {
            'probability_of_default': predictions['positive_probability'][0],
            'datarobot_model_id': selected_model.id,
            'credit_score': label_predictions[0],
            'last_updated': str(project.created.date()),
            'model_used': selected_model.model_type,
            'version': 1.0
        }
        logger.debug(result)
    return result

@click.command()
@click.option("--id", default="", help="model type (datarobot or local)")
def main(id):
    print(predict_dr(id))

if __name__ == '__main__':
    # find .env conf file automagically
    main()