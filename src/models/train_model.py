from dotenv import find_dotenv, load_dotenv
import logging
import logging.config
import re
import os
import datetime
from sklearn.model_selection import StratifiedKFold
import numpy as np
import pandas as pd
from imblearn.over_sampling import SVMSMOTE
from src.data.get_dataset import Dataset
from src.features.build_features import Features
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score
import datarobot as dr
import glob
from pathlib import Path
from dateutil import tz
import pickle
import click
from datetime import timezone


# datarobot token
dr.Client(config_path='/mnt/drconfig.yaml')
project_dir = Path(__file__).resolve().parents[2]


def train_dr():
    """ upload dataset to datarobot and start autopilot training """
    logger = logging.getLogger(__name__)
    create_dr_dataset()
    logger.info("start creating project ...")
    now = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M')
    project_name = 'credit-score-{}'.format(now)
    # partition method
    proj_partition = dr.StratifiedCV(holdout_pct=20, reps=5)
    proj = dr.Project.create(sourcedata='/mnt/data/processed/processed_dataset.csv',
                            project_name=project_name)

    logger.info("start creating feature lists ...")
    features = proj.get_features()
    categorical_features = [feature.name for feature in features if 'manual_num' not in feature.name]
    numerical_features = [feature.name for feature in features if 'manual_cat' not in feature.name]
    fcat = proj.create_featurelist('categorical_features', categorical_features)
    fnum = proj.create_featurelist('numerical_features', numerical_features)

    logger.info("starting autopilot ...")
    target_name = 'default'
    proj.set_target(target_name,
                    metric='AUC',
                    partitioning_method=proj_partition,
                    featurelist_id=fcat.id,
                    worker_count=8)

    logger.info("waiting for autopilot")
    proj.wait_for_autopilot(timeout=None, verbosity=1)
    logger.info("auto pilot finished ...")

    logger.info("getting model results")
    proj.unlock_holdout()
    results = {'project_id':[], 
        'model_id':[], 
        'validation': [], 
        'cross_validation': [], 
        'holdout': [],
        'model_type':[],
        'created_time':[],
        }
    for model in proj.get_models():
        results['project_id'].append(model.project_id)
        results['model_id'].append(model.id)
        results['validation'].append(model.metrics['AUC']['validation'])
        results['cross_validation'].append(model.metrics['AUC']['crossValidation'])
        results['holdout'].append(model.metrics['AUC']['holdout'])
        results['model_type'].append(model.model_type)
        results['created_time'].append(now)
        logger.info("model_type : {}, validation: {}, cross_validation: {}, holdout: {}".format(model.model_type,
            model.metrics['AUC']['validation'], 
            model.metrics['AUC']['crossValidation'], 
            model.metrics['AUC']['holdout']
        ))
    results = pd.DataFrame(results)
    results.to_csv('/mnt/data/processed/datarobot/datarobot_results_{}.csv'.format(now), 
        index=False)
    return results

def save_model():
    """ select the model and train for 100% full data then save into a pickle for predictions """
    logger = logging.getLogger(__name__)
    results = pd.concat([pd.read_csv(f) for f in glob.glob("/mnt/data/processed/datarobot/datarobot_results_*.csv")],
                      ignore_index=True)
    results = results.sort_values(['cross_validation'], ascending=False)
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(results)
    model_id = input('input the model id that you want to deploy :')
    selected_model = results[results['model_id'] == model_id]
    project_id = selected_model['project_id'].values[0]
    model_id = selected_model['model_id'].values[0]
    model = dr.Model.get(project=project_id,
                        model_id=model_id)
    logger.info("training on 100 % data..")
    model_job_id = model.train(sample_pct=100)
    selected_model = dr.models.modeljob.wait_for_async_model_creation(
        project_id, model_job_id)
    with open('/mnt/results/models/selected_model.pkl', 'wb') as sel_model:
        pickle.dump(selected_model, sel_model)
    logger.info("model saved")


def create_dr_dataset():
    """ build features for datarobot model """
    logger = logging.getLogger(__name__)
    dataset = Dataset()
    all_dataset = dataset.get_clean_dataset()
    feature_builder = Features(all_dataset)
    feature_builder.build_all_features()
    feature_builder.one_hot = []
    all_dataset = feature_builder.build_one_hot()
    logger.info("save features into local dataset...")
    all_dataset.to_csv('/mnt/data/processed/processed_dataset.csv', index=False)

def create_local_dataset():
    """ build features for local model """
    return True

def train_local():
    """ create features and train locally """
    dataset = Dataset()
    train_dataset, test_dataset = dataset.get_dataset()


def cross_validate_model(model, features, target, cv=5):
    """ upsampling the dataset and train cross validation model  """
    skf = StratifiedKFold(n_splits=cv, shuffle=True)
    result = {'val_auc': [], 'train_auc': []}
    for train_index, val_index in skf.split(np.zeros(len(features)), target):
        x_train, y_train = features.iloc[train_index], target.iloc[train_index]
        x_val, y_val = features.iloc[val_index], target.iloc[val_index]
        # oversampling
        sampling = SVMSMOTE(sampling_strategy=0.4)
        # train model
        x_train_res, y_train_res = sampling.fit_sample(x_train, y_train)  
        model.fit(x_train_res, y_train_res)
        y_train_scores = model.predict_proba(x_train_res)[:, 1]
        result['train_auc'].append(roc_auc_score(y_train_res,y_train_scores))
        #test on validation set
        y_val_scores = model.predict_proba(x_val.values)[:, 1]
        model.predict(x_val.values)
        plt.figure()
        sns.heatmap(confusion_matrix(y_val, model.predict(x_val.values)), 
                            annot=True, fmt="d")
        result['val_auc'].append(roc_auc_score(y_val, y_val_scores))
    return (pd.DataFrame(result))

@click.command()
@click.option("--model", default="datarobot", help="model type (datarobot or local)")
def main(model):
    if(model == 'datarobot'):
        train_dr()
        save_model()

if __name__ == '__main__':
    # find .env conf file automagically
    main()
