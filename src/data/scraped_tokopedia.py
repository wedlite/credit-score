from dotenv import find_dotenv, load_dotenv
import logging
import logging.config
import os
from pathlib import Path
from src.data import preprocess, utils
import pandas as pd

load_dotenv(find_dotenv())
project_dir = Path(__file__).resolve().parents[2]
logging.config.fileConfig('{}/logging_config.ini'.format(project_dir))

class ScrappedTokopedia:
    def __init__(self):
        self.dominant_category = None
        self.dominant_department = None
        self.products = None
        self.shop_info = None
        self.full_data = None

    def get_shop_data(self):
        query = """
            select 
                domain as shop_domain, 
                active_products
            from credit_score.crawled_shop_data
        """
        self.shop_info = utils.query_from_biggquery(query)
        return self.shop_info
    
    def get_products(self):
        self.get_products_by_category()
        self.get_products_by_department()
        self.products = pd.merge(self.dominant_category, self.dominant_department, on="shop_domain")
        return self.products

    def get_products_by_category(self):
        """ get the top category of the product that tokopedia merchant sold """
        self.dominant_category = self.get_dominant_products(category_depth=0)
        return self.dominant_category

    def get_products_by_department(self):
        """ get the top sub-category of the product that tokopedia merchant sold """
        self.dominant_department= self.get_dominant_products(category_depth=1)
        return self.dominant_department

    def get_dominant_products(self, category_depth=1):
        """ get the top product type sold for each tokopedia merchant either by category or sub category from tokopedia """
        category_depth = "department_name" if category_depth > 0 else "category_name"
        query =  """
            select 
                products.shop_domain,
                products.{category_depth} as dominant_{category_depth},
                products.num_products as num_dominant_{category_depth}
            from 
                (select 
                    *,  
                    ROW_NUMBER() OVER (PARTITION BY shop_domain ORDER BY num_products DESC) AS category_rank
                from (
                    select 
                        shop_domain, 
                        {category_depth}, count(1) as num_products
                    from
                        credit_score.shop_data_products
                    group by 
                        shop_domain, {category_depth}
                    order by 
                        shop_domain, num_products desc
                    )
                ) products
            where category_rank = 1
        """.format(category_depth=category_depth)
        return utils.query_from_biggquery(query)

    def get_full_data(self):
        self.get_shop_data()
        self.get_products()
        self.full_data = pd.merge(self.shop_info, self.products, on='shop_domain', how="left")
        return self.full_data