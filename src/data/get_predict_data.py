from dotenv import find_dotenv, load_dotenv
import pandas as pd
import numpy as np
import logging
import logging.config
import os
from pathlib import Path
from src.data import preprocess, utils
from src.data.crawler import TokopediaCrawler
from pandas.io.json import json_normalize
from sklearn.model_selection import StratifiedShuffleSplit
from src.features.build_features import Features
import pickle
import click

load_dotenv(find_dotenv())
project_dir = Path(__file__).resolve().parents[2]
logging.config.fileConfig('{}/logging_config.ini'.format(project_dir))

class PredictDataset:
    def __init__(self, application_id):
        self.application_id = application_id
        self.data = pd.DataFrame()
        self.clean_data = pd.DataFrame()
        self.required_columns = [
            'loan_amount',
            'loan_duration',
            'program_name',
            'complaint_count',
            'create_time',
            'cashflow_history',
            'transaction_history',
            'documents.ktp',
            'documents.npwp',
            'documents.bank_statement',
            'documents.verificationPhoto',
            'npwp',
            'personal_info.gender',
            'personal_info.marital_status',
            'personal_info.birth_date',
            'bank_account', 
            'successful_rate',
            'face_match.similarity',
            'reputation_count.negative',
            'shop_info.merchant_type',
            'shop_info.open_since',
            'shop_info.shop_type',
            'shop_info.date_start',
            'shop_info.shop_domain',
            'shop_info.shop_name',
            'confidence.address',
            'domicile.city',
            'ktp.address.city',
            'external_shop_info.shopee.username',
            'external_shop_info.bukalapak.username',
            'external_shop_info.shopee.total_avg_star',
            'other_income_source.monthly_income',
            'relative_info.relationship',
            'ktp.ktp_no',
            'total_transaction',
            'pefindo_score',
            'pefindo_grade',
            'moderation_count',
            'dueAmount',
            'outstandingAmount'
        ]
        self.logger = logging.getLogger(__name__)

    def get_predict_tokopedia_loan_data(self):
        """
        get tokopedia loan's data per application id
        """
        data = utils.query_from_datastore(kind='tokopediaLoanProposals', 
            filters={'application_id': self.application_id})
        if(len(data) > 0):
            data = preprocess.get_required_colums(data, self.required_columns)
            # rename column to the same aliases in training dataset
            data = data.rename(columns={
                                'npwp':'npwp_no', 
                                'personal_info.birth_date':'date_of_birth',
                                'confidence.address': 'confidence_address',
                                'external_shop_info.shopee.total_avg_star' : 'shopee_star',
                                'face_match.similarity' : 'face_match'
                                })
            # get valid npwp real time
            data['valid_npwp_name'] = data['npwp_no'].apply(preprocess.get_valid_npwp)
            # fill na city
            data["city"] = data["domicile.city"].fillna(data["ktp.address.city"])
            # turn shopee user to binary
            data["shopee_user"] = 0
            data.loc[~(data["external_shop_info.shopee.username"].isnull()), 'shopee_user'] = 1
            data["bukalapak_user"] = 0
            data.loc[~(data["external_shop_info.bukalapak.username"].isnull()), 'bukalapak_user'] = 1
            data = data.rename(columns=lambda x: x.split('.')[-1])
            # preprocess tokopedia data same as the training
            data = preprocess.clean_tokped_data(data)
            if(len(data) > 0):
                # get pefindo data
                data = data.apply(preprocess.get_pefindo_data, axis=1)
                # clean pefindo data
                self.logger.info("getting pefindo data")
                data = preprocess.clean_pefindo_data(data)
                self.logger.info("data acquired")
        self.data = data
        return self.data

    def get_clean_data(self):
        self.get_predict_tokopedia_loan_data()
        if(len(self.data) > 0):
            feature_builder = Features(self.data)
            self.clean_data = feature_builder.build_all_features()
        return self.clean_data

@click.command()
@click.option("--id", default="", help="model type (datarobot or local)")
def main(id):
    predict_data = PredictDataset(id)
    predict_data = predict_data.get_clean_data()
    return predict_data

if __name__ == '__main__':
    # find .env conf file automagically
    main()