from dotenv import find_dotenv, load_dotenv
import pandas as pd
import numpy as np
import logging
import logging.config
import os
from pathlib import Path
from src.data import preprocess, utils
from src.data.crawler import TokopediaCrawler
from src.data.scraped_tokopedia import ScrappedTokopedia
from pandas.io.json import json_normalize
from sklearn.model_selection import StratifiedShuffleSplit
import pickle


load_dotenv(find_dotenv())
project_dir = Path(__file__).resolve().parents[2]
logging.config.fileConfig('{}/logging_config.ini'.format(project_dir))


class Dataset:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.tokped_data = None
        self.crawled_data = None
        self.admin_data = None
        self.clean_data = None
        self.train_dataset = None
        self.test_dataset = None
        self.project_dir = project_dir
        self.cache = {
            'admin_data': '{}/data/raw/loan_default.csv'.format(self.project_dir),
            'tokped_data': '{}/data/raw/tokopedia_loan_proposals.pkl'.format(self.project_dir)
        }

    def get_admin_service_data(self):
        """ 
        Get loans' data with default information from admin-service database and save into loan_default.csv.
        """
        # cache's filename
        if(Path(self.cache['admin_data']).exists() == False):
            # Get the unpaid days and the penalty amount per invoice
            days_late = '''
                select 
                    loan_invoice_id,
                    count(*) as days_late, 
                    sum(amount) as penalty_amount
                from 
                    "Penalties"
                group by loan_invoice_id
            '''
            # TODO query description
            table_a = '''
                select 
                    "LoanProposals".tenor, 
                    "Loans".id,
                    "Loans".interest_rate, 
                    "Loans".status, 
                    "LoanProposals".credit_score,
                    "TokopediaLoanProposals".application_id, 
                    max(days_late) as days_late, 
                    max(penalty_amount) * 100 / avg("LoanInvoices".amount) as max_unpaid_percentage 
                from 
                    "Loans" 
                    inner join "LoanProposals" 
                        on ("LoanProposals".id = "Loans".proposal_id)
                    inner join "BorrowerEntities" 
                        on "BorrowerEntities".id = "LoanProposals".borrower_entity_id
                    inner join "LoanInvoices" 
                        on "LoanInvoices".loan_id = "Loans".id
                    left outer join "TokopediaLoanProposals" 
                        on "TokopediaLoanProposals".loan_proposal_id = "LoanProposals".id
                    left outer join ({0}) "DaysLate" 
                        on "DaysLate".loan_invoice_id = "LoanInvoices".id 
                group by 
                    "LoanProposals".tenor, 
                    "Loans".id, 
                    "TokopediaLoanProposals".application_id, 
                    "LoanProposals".credit_score
                order by max(days_late) desc
            '''.format(days_late)
            # TODO query description
            table_b = '''
                SELECT 
                    "TokopediaLoanProposals".application_id, 
                    COUNT(CASE WHEN "LoanInvoices".status = 'paid' then 1 ELSE NULL END) paid, 
                    COUNT(CASE WHEN "LoanInvoices".status = 'unpaid' then 1 ELSE NULL END) unpaid
                FROM 
                    "TokopediaLoanProposals"
                    INNER JOIN "LoanProposals" 
                        ON "LoanProposals".id = "TokopediaLoanProposals".loan_proposal_id
                    INNER JOIN "Loans" 
                        ON "Loans".proposal_id = "LoanProposals".id
                    INNER JOIN "LoanInvoices" 
                        ON "LoanInvoices".loan_id = "Loans".id
                WHERE (("LoanInvoices"."dueDate" <  (current_date - interval '30 day')))
                GROUP BY 1
            '''
            # TODO query description
            table_c = '''
                SELECT 
                    "TokopediaLoanProposals".application_id,	
                    "LoanProposals".approved_amount
                FROM 
                    "LoanProposals"
                inner join "TokopediaLoanProposals" 
                    ON "TokopediaLoanProposals".loan_proposal_id = "LoanProposals".id
            '''
            # TODO query description
            admin_service_query = '''
                select 
                    "A".application_id, 
                    "A".credit_score,
                    "C".approved_amount, 
                    "A".id as loan_id, 
                    "A".tenor,
                    "A".max_unpaid_percentage, 
                    "A".days_late, 
                    "A".interest_rate, 
                    "A".status, 
                    "B".paid, 
                    "B".unpaid, 
                    CASE 
                        WHEN ((("A".max_unpaid_percentage > 1) AND ("A".days_late > 31)) OR "B".unpaid > 0) 
                            THEN 1 ELSE 0 
                        END as default 
                from
                    ({0}) "A"
                    LEFT OUTER JOIN 
                    ({1}) "B"
                        ON "A".application_id = "B".application_id
                    INNER JOIN
                    ({2}) "C" 
                        ON "C".application_id = "A".application_id
            '''.format(table_a, table_b, table_c)
            data = utils.query_from_pg_admin(admin_service_query)
            data.to_csv(self.cache['admin_data'], index=False)
            self.logger.info('Download from admin service completed')
        else:
            self.logger.info('Use cache for admin service data')
            data = pd.read_csv(self.cache['admin_data'])
        self.admin_data = data
        return self.admin_data

    def get_tokopedia_loan_data(self):
        """
        Get and clean tokopedia loan proposal's data into a csv file.
        Notes : all data acquired from 'TokopediaLoanProposals' are limited only to recent 6 to 12 months
        """
        # cache's filename
        if(Path(self.cache['tokped_data']).exists() == False):
            query = """
                SELECT
                    tokped.application_id,
                    tokped.loan_amount,
                    tokped.loan_duration,
                    tokped.program_name,
                    tokped.complaint_count,
                    tokped.create_time,
                    tokped.cashflow_history,
                    tokped.transaction_history,
                    tokped.documents.ktp,
                    tokped.documents.npwp,
                    tokped.documents.bank_statement,
                    tokped.documents.verificationPhoto,
                    tokped.npwp as npwp_no,
                    tokped.personal_info.full_name,
                    tokped.personal_info.email,
                    tokped.personal_info.gender,
                    tokped.personal_info.marital_status,
                    tokped.personal_info.birth_date as date_of_birth,
                    tokped.bank_account,
                    CASE 
                        tokped.successful_rate.provided  
                    WHEN 
                        'integer'
                    THEN 
                        tokped.successful_rate.integer 
                    ELSE 
                        tokped.successful_rate.float 
                    END as successful_rate,
                    CASE 
                        tokped.face_match.similarity.provided  
                    WHEN 
                        'integer'
                    THEN 
                        tokped.face_match.similarity.integer 
                    ELSE 
                        tokped.face_match.similarity.float 
                    END as face_match,
                    tokped.reputation_count.negative,
                    tokped.shop_info.merchant_type,
                    tokped.shop_info.open_since,
                    tokped.shop_info.shop_type,
                    tokped.shop_info.date_start,
                    tokped.shop_info.shop_domain,
                    tokped.shop_info.shop_name,
                    tokped.confidence.address as confidence_address,
                    case when
                        tokped.domicile.city = ''
                    then
                        tokped.ktp.address.city 
                    else
                        tokped.domicile.city 
                    end as city,
                    case when
                        external_shop_info.shopee.username = ''
                    then 0
                    else 1 
                    end as shopee_user,
                    case when
                        external_shop_info.bukalapak.username = ''
                    then 0
                    else 1 
                    end as bukalapak_user,
                    CASE 
                        tokped.external_shop_info.shopee.total_avg_star.provided 
                    WHEN 
                        'integer'
                    THEN 
                        tokped.external_shop_info.shopee.total_avg_star.integer 
                    ELSE 
                        tokped.external_shop_info.shopee.total_avg_star.float 
                    END as shopee_star,
                    tokped.other_income_source.monthly_income,
                    tokped.relative_info.relationship,
                    tokped.ktp.ktp_no,
                    tokped.moderation_count,
                    tokped.total_transaction,
                    npwp.nama as valid_npwp_name,
                    Report.ContractSummary.Debtor.OutstandingAmountSum.entity.LocalValue as outstandingAmount,
                    Report.ContractSummary.Debtor.PastDueAmountSum.entity.LocalValue as dueAmount,
                    CASE WHEN
                        Report.CIP.RecordList.Record[ordinal(1)].grade is not null
                    THEN
                        Report.CIP.RecordList.Record[ordinal(1)].grade
                    ELSE
                        Report.GetCustomReportResult.CIP.RecordList.Record[ordinal(1)].grade
                    END as pefindo_grade,
                    CASE WHEN
                        Report.CIP.RecordList.Record[ordinal(1)].score is not null
                    THEN
                        Report.CIP.RecordList.Record[ordinal(1)].score
                    ELSE
                        Report.GetCustomReportResult.CIP.RecordList.Record[ordinal(1)].score
                    END as pefindo_score,
                    CASE WHEN
                        Report.CIP.RecordList.Record[ordinal(1)].trend is not null
                    THEN
                        Report.CIP.RecordList.Record[ordinal(1)].trend
                    ELSE
                        Report.GetCustomReportResult.CIP.RecordList.Record[ordinal(1)].trend
                    END as pefindo_trend,
                    CASE WHEN
                        Report.CIP.RecordList.Record[ordinal(1)].ProbabilityOfDefault is not null
                    THEN
                        Report.CIP.RecordList.Record[ordinal(1)].ProbabilityOfDefault
                    ELSE
                        Report.GetCustomReportResult.CIP.RecordList.Record[ordinal(1)].ProbabilityOfDefault
                    END as pefindo_prob_default
                FROM credit_score.tokopediaLoanProposals as tokped
                left join 
                    credit_score.npwp as npwp on tokped.npwp = npwp.npwp
                left join 
                    credit_score.pefindo_single pef_s on tokped.ktp.ktp_no = pef_s.__key__.name
                left join 
                    credit_score.pefindo_report pef_r on pef_s.Pefindoid = pef_r.__key__.name
            """
            data = utils.query_from_biggquery(query)
            if(len(data) > 0):
                # save as pickle due to complex type data
                with open(self.cache['tokped_data'], 'wb') as cache_file:
                    pickle.dump(data, cache_file)
        else:
            self.logger.info('Use cache for tokopedia loan proposals data')
            with open(self.cache['tokped_data'], 'rb') as cache_file:
                data = pickle.load(cache_file)
        self.tokped_data = data
        return self.tokped_data

    def get_tokopedia_crawled_data(self):
        """
        Get crawled tokopedia data
        """
        scraped_tokped = ScrappedTokopedia()
        self.crawled_data = scraped_tokped.get_full_data()
        return self.crawled_data

    def get_clean_dataset(self):
        """
        Merge tokopedia loan proposal's and admin services data by application id into dataset that 
        are ready to be analyzed for credit scoring model.
        """
        self.get_admin_service_data()
        self.get_tokopedia_loan_data()
        # remove null paid and unpaid
        self.admin_data = self.admin_data[~(self.admin_data['paid'].isnull()) | ~(self.admin_data['unpaid'].isnull())]
        self.tokped_data = preprocess.clean_tokped_data(self.tokped_data)
        self.tokped_data = preprocess.clean_pefindo_data(self.tokped_data)
        self.logger.info('Merge admin service and tokopedia loan proposals')
        self.clean_data = pd.merge(
            self.admin_data, self.tokped_data, on='application_id', how='inner')
        # crawler
        self.logger.info('Merge completed')
        return self.clean_data

    def get_dataset(self, random_state=1234):
        """ get cleaned dataset and split into training and test dataset """
        self.get_clean_dataset()
        dataset = self.clean_data
        ShuffleSplit = StratifiedShuffleSplit(n_splits=1, 
                                      test_size=0.25, 
                                      random_state=random_state)
        # split the dataset with balanced target
        # sklearn required number of features and target to split
        for train_index, test_index in ShuffleSplit.split(np.zeros(len(dataset)), 
                                                        dataset['default']):
            self.train_dataset = dataset.iloc[train_index]
            self.test_dataset  = dataset.iloc[test_index] 
        return self.train_dataset, self.test_dataset

def main():
    dataset = Dataset()
    dataset.get_clean_dataset()

if __name__ == '__main__':
    # find .env conf file automagically
    main()
