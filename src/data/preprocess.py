import logging
import logging.config
import pandas as pd
import numpy as np
from pathlib import Path
from src.data.crawler import TokopediaCrawler
from multiprocessing import Pool, cpu_count
import requests
import uuid

project_dir = Path(__file__).resolve().parents[2]

def check_name(row):
    "check whether the emails contains its full name"
    names = row['full_name'].split()
    email = row['email'].split('@')[0]
    match = 0
    for name in names:
        if(len(name) > 1):
            match = match + email.count(name.lower())
    return match

def change_program_name(data, col_name='program_name'):
    """ Change old program name like 'KMG Taralite' or 'KMK Taralite' to 'Dana Tara' """
    if(data[col_name].count() > 0):
        data[col_name] = data[col_name].fillna('Dana Tara')
        mask = data[col_name].str.strip().isin(['KMG Taralite', 'KMK Taralite'])
        data.loc[mask, col_name] = 'Dana Tara'
    return data


def change_marital_status(data, col_name='marital_status'):
    """ 
    Change the marital status to have same marital status name. 
    Marital status in TokopediaLoanProposals have different name for the same thing such as 'Kawin' and 'Menikah'.
    """

    status_replace = {
        'Belum Kawin': 'Belum Menikah',
        'Kawin': 'Menikah',
        'Cerai / Janda / Duda': 'Cerai'
    }
    data[col_name] = data[col_name].str.strip()
    data[col_name] = data[col_name].map(status_replace)
    data.loc[data[col_name].isnull(), col_name] = 'unspecified'
    return data

def remove_cashflow_error(data):
    """ There are data that the cashflow is zero due to tecnical issues. We will remove this kind of data. """
    data = data.loc[data['total_transaction'] > 0]
    return data

def change_to_binary(data, columns=[]):
    """
    Transform the values to binary. 0 if it is null and 1 if there are values.
    """
    for col in columns:
        data.loc[~data[col].isnull(), col] = 1
        data[col] = data[col].fillna(0)
    return data

def get_missing_open_since(data):
    """ Crawl the open since date from tokoepedia for the missing open since """
    cache = '{project_dir}/data/external/cache/{name}.csv'.format(project_dir=project_dir,
                                                                  name=str(uuid.uuid4()) + '_cache.csv')
    crawler = TokopediaCrawler()
    data['open_since'] = data['shop_domain'].apply(
        lambda x: crawler.get_open_since(x))
    logging.info("{} cached".format(cache))
    data.to_csv(cache)
    return data

def parallel_get_missing_open_since(data):
    """ Run the tokopedia crawler using multi core to increase the speed """
    logger = logging.getLogger(__name__)

    cache = '{project_dir}/data/external/missing_open_since.csv'.format(project_dir=project_dir)
    if(Path(cache).exists() == False):
        logger.info("starting parallel crawl")
        cores = cpu_count()  # Number of CPU cores on your system
        partitions = cores  # Define as many partitions as you want
        data_split = np.array_split(data, partitions)
        pool = Pool(cores)
        data = pd.concat(pool.map(get_missing_open_since, data_split))
        pool.close()
        pool.join()
        logger.info("parallel crawl finished")
        data.to_csv(cache, index=False, doublequote=True)
    else:
        logger.info('using crawled caches')
        data = pd.read_csv(cache)
    return data

def fill_missing_open_since(data):
    """ fill missing open since by crawled open since to the dataset """
    logger = logging.getLogger(__name__)

    missing_open_since = pd.DataFrame()
    missing_open_since['shop_domain'] = data[(data['open_since'].isnull()) &
                                             ~(data['shop_domain'].isnull())]['shop_domain'].unique()
    if(len(missing_open_since) > 0):
        crawled_missing = parallel_get_missing_open_since(missing_open_since)
        logger.info("fill missing open since")
        data = pd.merge(data, crawled_missing, on="shop_domain", how="left")
        data.open_since_x = data.open_since_x.fillna(data.open_since_y)
        data = data.drop(columns=['open_since_y'])
        data = data.rename(columns={'open_since_x': 'open_since'})
    return data

def get_num_of_bank(data, col_name='bank_account'):
    """ get distinct number of bank account from the owner name and the number of bank account """
    data['num_of_banks'] = data[col_name].apply(len)
    data = data.drop(columns=[col_name])
    return data

def get_valid_npwp(npwp):
    """ get valid npwp on real time in the cloud functions (sse pajak) """
    npwp_name = np.nan
    if(len(npwp) > 0):
        query = {'npwp':npwp}
        r = requests.get('https://us-central1-taralite-ekyc.cloudfunctions.net/checkNpwp', params=query)
        if (len(r.text) > 0):
            results = r.json()
            npwp_name = results[0]['nama'] if len(results) > 0 else np.nan
    return npwp_name

def get_pefindo_data(row):
    """ get pefindo data on real time in the cloud functions """
    query = {'ktp':row['ktp_no'], 'date': row['dateRequest']}
    r = requests.get('https://dev-pefindo.taralite.com/getpefindoscoring', params=query)
    if(len(r.text) > 0):
        if('multiple' not in r.text.lower()):
            results = r.json()
            if(len(results) > 0):
                row['pefindo_grade'] = results['grade']
                row['dueAmount'] = results['dueAmount']
                row['pefindo_score'] = results['score']
                row['outstandingAmount'] = results['outstandingAmount']
    return row

def get_required_colums(data, required_columns):
    """ quick and dirty fix for some missing columns in datastore """
    missing_col = set(required_columns) - set(data.columns)
    if(len(missing_col) > 0):
        for col in missing_col:
            data[col] = np.nan
    data = data[required_columns]
    return data

def get_year(row):
    """ because of unconsistent dateformat in tokopedia so we need to parse date ourself"""
    logger = logging.getLogger(__name__)
    row = str(row)
    year = None
    for date in row.replace('/','-').split('-'):
        date = date.strip()
        if(date.isdigit() and len(date) == 4):
            year = date
    if(year == None):
        year = 1987
    return year

def clean_tokped_data(data):
    """ Clean tokopedia data for analysis """
    logger = logging.getLogger(__name__)

    logger.info('Tokopedia data cleaning started')
    data = data.replace('', np.nan)
    # replace na with default values
    data['relationship'] = data['relationship'].fillna('unspecified')
    data[['monthly_income', 'confidence_address', 'shopee_star', 'shopee_user', 'bukalapak_user']] = data[['monthly_income', 
    'confidence_address', 'shopee_star', 'shopee_user', 'bukalapak_user']].apply(lambda x: x.fillna(0))
    data['merchant_type'] = data['merchant_type'].fillna('Regular Merchant')
    data['shop_type'] = data['shop_type'].fillna('Hanya Online')

    # remoce birth date that contains unspecified
    data['date_of_birth'] = data['date_of_birth'].apply(get_year)
    data = change_program_name(data)
    data = change_marital_status(data)
    data = remove_cashflow_error(data)
    data = get_num_of_bank(data)
    # change successful_rate that are more than one into percentage
    data.loc[data['successful_rate'] > 1, 'successful_rate'] /= 100
    # cahnge to cateogry type
    data[['program_name', 'relationship','marital_status',
        'merchant_type','shop_type']] = data[['program_name', 'relationship','marital_status', 'merchant_type','shop_type']].astype('category')
    data = change_to_binary(data, 
        columns=['bank_statement', 'ktp', 'npwp', 'valid_npwp_name', 'verificationPhoto'])
    data = fill_missing_open_since(data)
    # remove demo store
    data = data[data['shop_name'] != 'DEMO STORE']
    logger.info('Tokopedia data cleaning completed')
    
    return data

def clean_pefindo_data(data):
    """ clean pefindo data for analysis """
    logger = logging.getLogger(__name__)

    logger.info('Pefindo data cleaning started')
    data = data.replace('', np.nan)
    # replace na with default values
    data[['pefindo_score', 'outstandingAmount']] = data[['pefindo_score', 'outstandingAmount']].apply(lambda x: x.fillna(0))
    # fill null values to No Data
    data['pefindo_grade'] = data['pefindo_grade'].fillna('No Data')
    # change Grade XX to No Data
    mask = data['pefindo_grade'].str.strip().isin(['XX'])
    data.loc[mask, 'pefindo_grade'] = 'No Data'
    logger.info('Pefindo data cleaning completed')
    return data
