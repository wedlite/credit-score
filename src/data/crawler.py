import requests
import json
import logging
import logging.config
import datetime
from pathlib import Path
from bs4 import BeautifulSoup

project_dir = Path(__file__).resolve().parents[2]
logging.config.fileConfig('{}/logging_config.ini'.format(project_dir))


class BaseCrawler:
    def select_attributes(self, attributes, data):
        if isinstance(data, dict):
            return dict((attr, data[attr]) for attr in attributes)
        elif isinstance(data, object):
            return dict((attr, getattr(data, attr)) for attr in attributes)

    def remove_or_updateAttributes(self, attributes, data):
        if isinstance(data, list):
            for row in data:
                self.removeOrUpdateAttributes(attributes, row)
        elif isinstance(data, (dict, object)):
            for attr in attributes:
                self.removeOrUpdateAttribute(attr, data)
        return data

    def remove_or_update_attribute(self, attr, data):
        if isinstance(attr, str):
            if attr in data:
                del data[attr]
        elif isinstance(attr, dict):
            key = attr['attr']
            f = attr['f']
            if key in data:
                data[key] = f(data[key])


class TokopediaCrawler(BaseCrawler):
    def __init__(self):
        self.name = 'Tokopedia Crawler'
        self.logger = logging.getLogger(__name__)
        self.default_request_headers = {
            'Origin': 'https://www.tokopedia.com',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        }
        self.domain = None
        self.shop_id = None
        self.open_since = None

    @property
    def result(self):
        attrs = [
            'shop_id',
            'open_since'
        ]
        return dict((attr, getattr(self, attr)) for attr in attrs)

    def __str__(self):
        return json.dumps(self.result, indent=2)

    def convert_numeral_string(self, number, f):
        return f(''.join(number.split('.')))

    def crawl_by_domain(self, domain):
        self.domain = domain
        return self.get_store_information()

    def get_open_since(self, domain):
        self.domain = domain
        self.get_store_information()
        with open("{}/data/external/cache/tokped.cache".format(project_dir), "a") as myfile:
            myfile.write("{domain},{open_since}\n".format(domain=self.domain, open_since=self.open_since))
        return self.open_since

    def get_store_information(self):
        self.logger.info('Getting {store} information data...'.format(store=self.domain))
        if('https://www.tokopedia.com' in self.domain):
            response = requests.get(self.domain)
            soup = BeautifulSoup(response.text, 'html.parser')
            # shop id
            shop_el = soup.find('input', id='shop-id')
            if(shop_el is not None):
                self.shop_id = shop_el.attrs['value']
            # open since
            open_el = soup.find(
                'i', {'data-original-title': 'Buka sejak'})
            if(open_el is not None):
                self.open_since = open_el.parent.get_text().strip()
            self.logger.debug('Shop ID : {shop_id}, Open Since : {open_since}'.format(
                shop_id=self.shop_id, open_since=self.open_since))
        return self.shop_id
