from dotenv import find_dotenv, load_dotenv
import pandas as pd
import numpy as np
from pathlib import Path
import logging
import logging.config
from scipy import stats
from src.data.get_dataset import Dataset
from pandas.api.types import CategoricalDtype

load_dotenv(find_dotenv())
project_dir = Path(__file__).resolve().parents[2]
logging.config.fileConfig('{}/logging_config.ini'.format(project_dir))


class Features:
    """
    There are features that are both categorical and numeric used for model evaluation. 
    
    The prefix manual_cat means that the features is manual transformed into categorical derived 
    from the original numeric type using tree analysis. this feature are optional to manual_num (we can remove the 
    prefix manual_cat features if we want to use numeric instead ) 

    The prefix manual_num means that the features are numerical at first. ( we can remove the prefix manual_num 
    if we want to use the categorical features instead ) 

    The prefix num means the features are numerical type and are mandatory. The prefix cat means the features are
    categorical type and are mandatory.

    The prefix columns is used for easier remove and transform columns (such as one hot encoding) when doing 
    model development.
    """
    def __init__(self, dataset, one_hot=[]):
        self.logger = logging.getLogger(__name__)
        self.dataset = dataset
        self.features = pd.DataFrame()
        self.one_hot = one_hot
        if 'default' in dataset:
            self.features['default'] = dataset['default']

    def build_loan_amount(self):
        """ preprocess loan amount values by binned into categorical value """
        self.logger.info('Building loan amount..')
        self.dataset['loan_amount_in_milion'] = self.dataset['loan_amount'] / 1000000
        self.features['manual_num_loan_amount_without_extreme'] = self.dataset['loan_amount_in_milion']
        # cut extreme values
        self.features.loc[self.features['manual_num_loan_amount_without_extreme']
                          > 25, "manual_num_loan_amount_without_extreme"] = 25
        # bin loan amount into categorical value
        self.features['manual_cat_loan_amount'] = pd.cut(
            self.features['manual_num_loan_amount_without_extreme'], [0, 6, 12, 25])
        # get numeric code
        self.features['manual_cat_loan_amount'] = self.features['manual_cat_loan_amount'].cat.codes

    def build_installment(self):
        """ create installment from loan amount and duration """
        self.logger.info('Building installment..')
        self.features['manual_num_installment'] = self.dataset['loan_amount_in_milion'] / self.dataset['loan_duration']
        self.features.loc[self.features['manual_num_installment'] > 4, "manual_num_installment"] = 4
        # bin installment into categorical value
        self.features['manual_cat_installment'] = pd.cut(self.features['manual_num_installment'], [
                                                  0, 1, 4])
        self.features['manual_cat_installment'] = self.features['manual_cat_installment'].cat.codes

    def build_loan_duration(self):
        """ binned loan duration into 3 category """
        self.logger.info('Building loan duration..')
        self.features['cat_ordinal_loan_duration'] = pd.cut(
            self.dataset['loan_duration'], [0, 3, 6, 18]).cat.codes

    def build_store_age(self):
        """ derive store age on months based on create time and open since """
        self.logger.info('Building store age..')
        self.features['manual_num_store_age'] = (pd.to_datetime(
            self.dataset['create_time']).dt.date - pd.to_datetime(self.dataset['open_since']).dt.date).dt.days / 30
        self.features.loc[self.features['manual_num_store_age'] > 48, "manual_num_store_age"] = 48
        self.features['manual_cat_store_age'] = pd.cut(self.features['manual_num_store_age'], [
                                                6, 24, 48]).cat.codes

    def build_documents(self):
        """ get documents from dataset """
        self.logger.info('Building Documents..')
        self.features[['bank_statement', 'ktp', 'npwp', 'valid_npwp_name']] = self.dataset[[
            'bank_statement', 'ktp', 'npwp', 'valid_npwp_name']]

    def build_monthly_income(self):
        """ get other monthly income and binmed into categories """
        self.logger.info('Building monthly income..')
        self.features['manual_num_monthly_income_in_milion'] = self.dataset['monthly_income'] / 1000000
        self.features.loc[self.features['manual_num_monthly_income_in_milion'] > 20, "manual_num_monthly_income_in_milion"] = 20
        self.features['manual_cat_monthly_income_in_milion'] = pd.cut(
            self.features['manual_num_monthly_income_in_milion'], [float("-inf"), 0, 8, 20])
        self.features['manual_cat_monthly_income_in_milion'] = self.features['manual_cat_monthly_income_in_milion'].cat.codes

    def build_complaint_ratio(self):
        """ calculate complaint over total transaction from the past 6 months and binned into categories """
        self.logger.info('Building complaint ratio..')
        self.features['manual_num_complaint_ratio'] = self.dataset['complaint_count'] / self.dataset['total_transaction']
        self.features['manual_cat_complaint_ratio'] = pd.cut(
            self.features['manual_num_complaint_ratio'], [float("-inf"), 0.008, 0.025, float("inf")])
        self.features['manual_cat_complaint_ratio'] = self.features['manual_cat_complaint_ratio'].cat.codes

    def build_negative_ratio(self):
        """ calculate negative reputation over total transaction and binned into categories """
        self.logger.info('Building negative ratio..')
        self.features['manual_num_negative_ratio'] = self.dataset['negative'] / self.dataset['total_transaction']
        self.features['manual_cat_negative_ratio'] = pd.cut(
            self.features['manual_num_negative_ratio'], [float("-inf"), 0.003, 0.008, float("inf")])
        self.features['manual_cat_negative_ratio'] = self.features['manual_cat_negative_ratio'].cat.codes

    def build_shop_info(self):
        """ get shop information as category values """
        self.logger.info('Building shop info..')
        # cannot use cat codes due to single rows when predict
        program_replace = {
            'Dana Tara': 0,
            'Preapproved': 1
        }
        merchant_replace = {
            'Gold Merchant': 0,
            'Regular Merchant': 1
        }
        shop_replace = {
            'Ada Toko Fisik': 0,
            'Hanya Online': 1
        }
        # self.features['program_name'] = self.dataset['program_name'].map(program_replace)
        self.features['merchant_type'] = self.dataset['merchant_type'].map(merchant_replace)
        self.features['shop_type'] = self.dataset['shop_type'].map(shop_replace)
        self.features['shopee_user'] = self.dataset['shopee_user']
        self.features['bukalapak_user'] = self.dataset['bukalapak_user']
        self.features['manual_num_shopee_star'] = self.dataset['shopee_star']
        self.features['manual_cat_shopee_star'] = pd.cut(self.features['manual_num_shopee_star'], [float("-inf"),0,4.5,6])
        self.features['manual_cat_shopee_star'] = self.features['manual_cat_shopee_star'].cat.codes
        # self.features['successful_rate'] = self.dataset['successful_rate']

    def build_personal_info(self):
        """ get personal borrower information features such as age """
        self.logger.info('Building personal info..')
        # cannot use cat codes due to single rows when predict
        marital_replace = {
            'unspecified': 1,
            'Belum Menikah': 1,
            'Menikah': 2,
            'Cerai': 3
        }
        self.features['cat_marital_status'] = self.dataset['marital_status'].map(marital_replace)
        self.features['cat_marital_status'] = self.features['cat_marital_status'].astype(CategoricalDtype(categories=list(set(marital_replace.values())), ordered=False))
        # calculate user age
        create_time = pd.to_datetime(self.dataset['create_time']).dt.year
        dob = self.dataset['date_of_birth'].astype(int)
        self.features['manual_num_age'] = create_time - dob
        self.features['manual_cat_age'] = pd.cut(
            self.features['manual_num_age'], [0, 21, 35, 200]).cat.rename_categories([1,3,2]).astype(int)
        # change num of banks to binary
        self.features['num_of_banks'] = self.dataset['num_of_banks']
        self.features.loc[self.features['num_of_banks']> 0, 'num_of_banks'] = 1

    def build_cashflow(self):
        """ calculate cashflow information as features """
        self.logger.info('Building cashflow..')
        # last 6 months median revenue before propose loans
        self.features['manual_num_last_6m_median_cash'] = self.dataset['cashflow_history'].str[:6].apply(np.median)
        self.features['manual_num_last_6m_median_cash'] = self.features['manual_num_last_6m_median_cash'] / 1000000

        self.features.loc[self.features['manual_num_last_6m_median_cash'] > 24, "manual_num_last_6m_median_cash"] = 24
        self.features['manual_cat_last_6m_median_cash'] = pd.cut(
            self.features['manual_num_last_6m_median_cash'], [0,6,24])
        self.features['manual_cat_last_6m_median_cash'] = self.features['manual_cat_last_6m_median_cash'].cat.codes

        # last 12 months average revenue before propose loans
        self.features.loc[:, 'manual_num_cashflow_average'] = self.dataset['cashflow_history'].apply(np.mean)
        self.features['manual_num_cashflow_average'] = self.features['manual_num_cashflow_average'] / 1000000
        self.features.loc[self.features['manual_num_cashflow_average'] > 24, "manual_num_cashflow_average"] = 24

        # last months revenue 
        self.features.loc[:, 'manual_num_last_months_cash'] = self.dataset['cashflow_history'].str[-1]
        self.features['manual_num_last_months_cash'] = self.features['manual_num_last_months_cash'] / 1000000
        self.features.loc[self.features['manual_num_last_months_cash'] > 24, "manual_num_last_months_cash"] = 24
        self.features['manual_cat_last_months_cash'] = pd.cut(
            self.features['manual_num_last_months_cash'], [0,6,24]).cat.codes

        # proposed loan amount over 12 months average revenue
        self.features['manual_num_cashflow_ratio'] = self.dataset['loan_amount_in_milion'] / self.features['manual_num_cashflow_average']
        self.features.loc[self.features['manual_num_cashflow_ratio'] > 2, "manual_cat_cashflow_ratio"] = 2
        self.features['manual_cat_cashflow_ratio'] = pd.cut(
            self.features['manual_num_cashflow_ratio'], [0,1,2,float("inf")]).cat.codes

        # 12 months cashflow features derived with ordinary least square regression
        self.dataset['cashflow_linreg']     = self.dataset['cashflow_history'].apply(olr, norm = True)
        self.dataset['cashflow_linreg_without_norm']     = self.dataset['cashflow_history'].apply(olr, norm = False)
        self.features['manual_num_cashflow_slope']     = self.dataset['cashflow_linreg'].str[0]
        self.features['manual_num_cashflow_intercept'] = self.dataset['cashflow_linreg'].str[1]
        self.features['manual_num_cashflow_rsquared']  = self.dataset['cashflow_linreg'].str[2]
        self.features['manual_num_cashflow_downrate']  = self.dataset['cashflow_linreg'].str[3]
        self.features['manual_num_cashflow_stable']    = self.dataset['cashflow_linreg'].str[5]
        self.features['manual_num_cashflow_slope_without_norm'] = self.dataset['cashflow_linreg_without_norm'].str[0]

        # trasform to category 
        self.features['manual_cat_cashflow_slope'] = pd.cut(
            self.features['manual_num_cashflow_slope'], [float("-inf"),0,0.05,float("inf")])
        self.features.loc[self.features['manual_num_cashflow_slope'] > 0.05, "manual_num_cashflow_slope"] = 0.05
        self.features['manual_cat_cashflow_slope'] = self.features['manual_cat_cashflow_slope'].cat.codes
        self.features['manual_cat_cashflow_rsquared'] = pd.cut(
            self.features['manual_num_cashflow_rsquared'], [0,0.3,0.5,1])
        self.features['manual_cat_cashflow_rsquared'] = self.features['manual_cat_cashflow_rsquared'].cat.codes
        self.features['manual_cat_cashflow_downrate'] = pd.cut(
            self.features['manual_num_cashflow_downrate'], [0,3,12])
        self.features['manual_cat_cashflow_downrate'] = self.features['manual_cat_cashflow_downrate'].cat.codes
        self.features['manual_cat_cashflow_stable'] = pd.cut(
            self.features['manual_num_cashflow_stable'], [0,3,12])
        self.features['manual_cat_cashflow_stable'] = self.features['manual_cat_cashflow_stable'].cat.codes

    def build_pefindo(self):
        self.logger.info('Building pefindo..')
        # regroup pefindo grade
        self.features.loc[self.dataset['pefindo_grade'].str[0].str.contains('D|E'), 'cat_pefindo_grade'] = 'C'
        self.features.loc[self.dataset['pefindo_grade'].str[0].str.contains('C'), 'cat_pefindo_grade'] = 'B'
        self.features.loc[self.dataset['pefindo_grade'].str.strip().isin(['B2','B3']), 'cat_pefindo_grade'] = 'B'
        self.features.loc[self.dataset['pefindo_grade'].str.strip().isin(['B1']), 'cat_pefindo_grade'] = 'A'
        self.features['cat_pefindo_grade'] = self.features['cat_pefindo_grade'].fillna('No Data')
        grade_replace = {
            'A': 0,
            'B': 1,
            'C': 2,
            'No Data':3
        }
        self.features['cat_pefindo_grade'] = self.features['cat_pefindo_grade'].map(grade_replace)
        # get pefindo score
        self.features['manual_num_pefindo_score'] = self.dataset['pefindo_score'].astype(int)
        self.features.loc[self.features['manual_num_pefindo_score'] > 700, "manual_num_pefindo_score"] = 700
        self.features['manual_cat_pefindo_score'] = pd.cut(self.features['manual_num_pefindo_score'].astype(int), [200,500,670,800,999])
        self.features['manual_cat_pefindo_score'] = self.features['manual_cat_pefindo_score'].cat.codes.astype(int)

        # transform due amount to category
        self.dataset['dueAmount'] = self.dataset['dueAmount'].astype(float).fillna(0)
        self.features.loc[self.dataset['dueAmount'] > 0, 'cat_due_amount'] = 1
        self.features.loc[self.dataset['dueAmount'] <= 0, 'cat_due_amount'] = 0
        self.features['cat_due_amount'] = self.features['cat_due_amount']

        # transform due amount to category
        self.dataset['outstandingAmount'] = self.dataset['outstandingAmount'].astype(float).fillna(0)
        self.features.loc[self.dataset['outstandingAmount'] > 0, 'cat_outstanding_amount'] = 1
        self.features.loc[self.dataset['outstandingAmount'] <= 0, 'cat_outstanding_amount'] = 0
        self.features['cat_outstanding_amount'] = self.features['cat_outstanding_amount']

    def build_transaction(self):
        self.features['manual_num_last_6m_median_transaction'] = self.dataset['transaction_history'].str[:6].apply(np.median)
        self.features['manual_cat_last_6m_median_transaction'] = pd.cut(self.features['manual_num_last_6m_median_transaction'], 
                                                    [0,30,50,70,100, float("inf")])
        self.features['manual_cat_last_6m_median_transaction'] = self.features['manual_cat_last_6m_median_transaction'].cat.codes

    def build_all_features(self):
        self.build_loan_amount()
        self.build_loan_duration()
        self.build_installment()
        self.build_store_age()
        self.build_documents()
        self.build_monthly_income()
        self.build_complaint_ratio()
        self.build_negative_ratio()
        self.build_shop_info()
        self.build_personal_info()
        self.build_cashflow()
        self.build_pefindo()
        self.build_transaction()
        self.logger.info('Features build finished')
        return self.features

    def build_one_hot(self):
        if(len(self.one_hot) > 0):
            self.features = self.get_dummies(self.features, self.one_hot)
        return self.features

    def get_dummies(self, data, columns):
        for col_name in columns:
            data = pd.concat([data.drop([col_name],1), 
                                pd.get_dummies(data[col_name], prefix=col_name)], axis=1)
        return data

def olr(y, sigma=0.1, months=12, norm=False):
    """ train ordinary least squares for cashflow to know the slope and intercept """
    x = np.array(range(months, 0, -1))
    if(norm):
        y = np.array(y[:months]) / max(y)
    else:
        y = np.array(y) / 1000000
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    r_squared = r_value ** 2
    # number of months that the revenue are lower than the linear limit
    lower_limit = (intercept - sigma) + (slope * x)
    down_rate = np.sum(lower_limit > y)
    # number of months that the revenue are higher than the linear limit
    upper_limit = (intercept + sigma) + (slope * x)
    up_rate = np.sum(upper_limit < y)
    stable_rate = months - (up_rate + down_rate)
    return [slope, intercept, r_squared, down_rate, up_rate, stable_rate]

def approx_transaction(row):
    cash_arr = np.array(row['cashflow_history'])
    trans_arr = np.array(row['transaction_history'])
    row['approx_transactions'] = np.nan_to_num(cash_arr / trans_arr)
    return row

def main():
    dataset = Dataset()
    train_data, test_data = dataset.get_dataset()
    features = Features(train_data)
    dataset.get_clean_dataset()


if __name__ == '__main__':
    # find .env conf file automagically
    main()
