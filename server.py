from flask import Flask,request,jsonify
from src.models import predict_model


app = Flask(__name__)

@app.route('/predict/<application_id>')
def predict(application_id):
    pred_result = predict_model.predict_dr(application_id)
    return jsonify(pred_result)

if __name__ == '__main__':
    app.run(debug=False, port=8888, host="0.0.0.0")