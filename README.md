Credit Score Model
==============================

Credit score model for predicting defaulted loan using machine learning

Project folder structure inspired on the 
[cookiecutter data science project template]("https://drivendata.github.io/cookiecutter-data-science/">)

Quick Start
==============================
# Requirements
- Docker (https://docs.docker.com/docker-for-mac/install/)

# Getting Started

## To start the project, build and run the docker first:
Build the docker image first:
```
docker-compose build bash
```
Then start the container in the background:
```
docker-compose start -d bash
```
Check your running container id first
```
docker ps
```
Get into the docker terminal by exec:
```
docker exec -it <container-id> bash
```
When in the terminal run jupyter notebook in background by:
```
nohup bash run_jupyter.sh & > dev/null
```

Access the jupyter notebook in your browser on localhost:8888

Notes: use exec to avoid re-running the container so you don't need to rebuild the images when you have some changes in development

## In the docker terminal

Use the GNUMake to get the dataset. Run 
```
make help
```
to get all the possible commands with its documentation.