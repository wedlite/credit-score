FROM ubuntu:16.04

RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip \
  && apt-get install -y locales \
  && apt-get install -y pandoc \
  && apt-get install -y texlive-xetex

RUN update-locale
RUN apt-get -y install \
    graphviz \
    git \
    make \
    vim \
    curl \
    lsb-core \
    python-joblib
ADD requirements.txt /mnt/requirements.txt
WORKDIR /mnt
ADD . /mnt
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
RUN export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" && \
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update -y && apt-get install google-cloud-sdk -y
EXPOSE 8888 5000
RUN export LC_ALL=C.UTF-8
RUN export LANG=C.UTF-8
RUN export FLASK_APP=server.py
ENTRYPOINT ["python", "server.py"]