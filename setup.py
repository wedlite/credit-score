from setuptools import find_packages, setup

setup(
    name='credit_model',
    packages=find_packages(),
    version='0.1.0',
    description='Credit score model for predicting defaulted loan using machine learning',
    author='Taralite Data Science',
    packages_dir='src',
    license='',
)
